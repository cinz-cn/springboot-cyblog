package cn.cinz.cyxx.service;

import cn.cinz.cyxx.model.User;
import org.apache.ibatis.annotations.Param;

/**
* 用户service
* @ClassName UserService
* @Description 用户service
* @Author zengcheng
* @Date 2021/10/4 10:23
* @Version 1.0
*/
public interface UserService {

    /**
     * 根据用户id获取用户信息
     * @param id
     * @return
     */
    User getUserinfo(@Param("id") String id) throws Exception;

    /**
     * 修改用户信息
     * @param user
     * @return
     */
    void editUserInfo(User user) throws Exception;

}
