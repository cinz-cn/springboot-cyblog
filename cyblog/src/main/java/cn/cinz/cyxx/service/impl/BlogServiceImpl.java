package cn.cinz.cyxx.service.impl;

import cn.cinz.cyxx.mapper.BlogMapper;
import cn.cinz.cyxx.model.*;
import cn.cinz.cyxx.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (描述功能模块)
 *
 * @Author: zengcheng
 * @Description: (内容作用)
 * @Date: 2021/9/11 22:23
 */
@Service
public class BlogServiceImpl implements BlogService {

    @Autowired
    private BlogMapper blogMapper;

    @Override
    public List<BlogAdd> selectBlogList(Blog blog) throws Exception {
        return blogMapper.selectBlogList(blog);
    }

    @Override
    public Blog selectBlog(Blog blog) throws Exception {
        return blogMapper.selectBlog(blog);
    }

    @Override
    public Integer countBlog(String id) {
        return blogMapper.countBlog(id);
    }

    @Override
    public List<Blog> selectMyBlogList(MyBlog myBlog) throws Exception {
        return blogMapper.selectMyBlogList(myBlog);
    }

    @Override
    public int updateByPrimaryKey(Blog blog) throws Exception {
        return blogMapper.updateByPrimaryKey(blog);
    }

    @Override
    public void updateBlogPageViews(String id) throws Exception {
        blogMapper.updateBlogPageViews(id);
    }

    @Override
    public int deleteBlog(Blog blog) throws Exception {
        return blogMapper.deleteBlog(blog);
    }

    @Override
    public int insertBlog(Blog blog) throws Exception {
        return blogMapper.insertBlog(blog);
    }
}
