package cn.cinz.cyxx.service.impl;

import cn.cinz.cyxx.common.enums.EUser;
import cn.cinz.cyxx.mapper.LoginMapper;
import cn.cinz.cyxx.model.User;
import cn.cinz.cyxx.service.LoginService;
import cn.cinz.cyxx.util.IdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 登陆验证 注册用户
 *
 * @Author: zengcheng
 * @Description: 登陆验证 注册用户
 * @Date: 2021/9/6 23:19
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private LoginMapper loginMapper;

    /**
     * 手机号用户
     * 验证是否注册  未注册则直接注册，已注册则登陆
     */
    @Override
    public User insertUser(User user) {
        user.setLogoutStatus(EUser.正常.getValue());
        User userS = loginMapper.selectUser(user);

        if (userS != null){
            return userS;
        }else {
            user.setId(IdUtils.getId());
            user.setRegisterDate(new Date());
            user.setAuthority(EUser.普通用户.getValue());
            loginMapper.insertUser(user);
            userS = loginMapper.selectUser(user);
            return userS;
        }
    }


    /**
     * QQ用户
     * 验证是否注册  未注册则直接注册，已注册则登陆
     */
    @Override
    public User insertQQUser(User user) {
        User userS = loginMapper.selectQQUser(user.getId());

        if (userS != null){
            return userS;
        }else {
            user.setId(user.getId());
            user.setSex(user.getSex());
            user.setRegisterDate(new Date());
            user.setAuthority(EUser.普通用户.getValue());
            user.setImg(user.getImg());
            loginMapper.insertQQUser(user);
            userS = loginMapper.selectQQUser(user.getId());
            return userS;
        }
    }
}
