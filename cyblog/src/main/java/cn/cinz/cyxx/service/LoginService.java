package cn.cinz.cyxx.service;

import cn.cinz.cyxx.model.User;

/**
 * (描述功能模块)
 *
 * @Author: zengcheng
 * @Description: (内容作用)
 * @Date: 2021/9/6 23:19
 */
public interface LoginService {

    /**
     * 注册
     * @param user
     * @return
     */
    User insertUser(User user);

    /**
     * qq注册
     * @param user
     * @return
     */
    User insertQQUser(User user);


}
