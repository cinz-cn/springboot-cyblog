package cn.cinz.cyxx.service.impl;

import cn.cinz.cyxx.mapper.UserMapper;
import cn.cinz.cyxx.model.User;
import cn.cinz.cyxx.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User getUserinfo(String id) throws Exception  {
        return userMapper.getUserinfo(id);
    }

    @Override
    public void editUserInfo(User user) throws Exception {
        userMapper.editUserInfo(user);
    }
}
