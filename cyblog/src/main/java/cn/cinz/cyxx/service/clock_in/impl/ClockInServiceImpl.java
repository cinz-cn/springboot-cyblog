package cn.cinz.cyxx.service.clock_in.impl;

import cn.cinz.cyxx.mapper.clock_in.ClockInMapper;
import cn.cinz.cyxx.model.clock_in.ClockIn;
import cn.cinz.cyxx.model.clock_in.ClockInExample;
import cn.cinz.cyxx.service.clock_in.ClockInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description 打卡功能实现
 * @Author zengcheng
 * @Date 2021/10/22 9:57
 * @Version 1.0
 */

@Service
public class ClockInServiceImpl implements ClockInService {

    @Autowired
    private ClockInMapper clockInMapper;


    @Override
    public List<ClockIn> selectClockIn(ClockIn clockIn) throws Exception {
        return clockInMapper.selectClockIn(clockIn);
    }

    @Override
    public int countByExample(ClockInExample example) throws Exception {
        return clockInMapper.countByExample(example);
    }

    @Override
    public int deleteByExample(ClockInExample example) throws Exception {
        return clockInMapper.deleteByExample(example);
    }

    @Override
    public int deleteByPrimaryKey(String id) throws Exception {
        return clockInMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(ClockIn record) throws Exception {
        return clockInMapper.insert(record);
    }

    @Override
    public int insertSelective(ClockIn record) throws Exception {
        return clockInMapper.insertSelective(record);
    }

    @Override
    public List<ClockIn> selectByExample(ClockInExample example) throws Exception {
        return clockInMapper.selectByExample(example);
    }

    @Override
    public ClockIn selectByPrimaryKey(String id) throws Exception {
        return clockInMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByExampleSelective(ClockIn record, ClockInExample example) throws Exception {
        return clockInMapper.updateByExampleSelective(record, example);
    }

    @Override
    public int updateByExample(ClockIn record, ClockInExample example) throws Exception {
        return clockInMapper.updateByExample(record, example);
    }

    @Override
    public int updateByPrimaryKeySelective(ClockIn record) throws Exception {
        return clockInMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(ClockIn record) throws Exception {
        return clockInMapper.updateByPrimaryKey(record);
    }
}
