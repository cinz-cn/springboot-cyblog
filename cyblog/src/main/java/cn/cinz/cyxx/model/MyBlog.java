package cn.cinz.cyxx.model;

/**
 * 描述
 *
 * @Description: TOO描述
 * @Author zengcheng
 * @Date 2021/10/10 17:59
 * @Version 1.0
 */
public class MyBlog {

    private BlogQuery blogQuery;
    private User user;

    public BlogQuery getBlogQuery() {
        return blogQuery;
    }

    public void setBlogQuery(BlogQuery blogQuery) {
        this.blogQuery = blogQuery;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
