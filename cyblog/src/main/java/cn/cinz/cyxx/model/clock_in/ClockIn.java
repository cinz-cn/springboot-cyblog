package cn.cinz.cyxx.model.clock_in;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 打卡实体
 *
 * @Description: 打卡实体
 * @Author zengcheng
 * @Date 2021/10/21 20:46
 * @Version 1.0
 */
public class ClockIn implements Serializable{

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "打卡日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date date;

    @ApiModelProperty(value = "开始时间")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    @ApiModelProperty(value = "结束时间")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    @ApiModelProperty(value = "实际时间")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date actualTime;

    @ApiModelProperty(value = "打卡状态，0 未打卡 1 已打卡 ")
    private String status;

    @ApiModelProperty(value = "补签状态/描述，2已补签 3未补签 null无需补签")
    private String retroactive;
    
    @ApiModelProperty(value = "用户id")
    private String uId;

    private static final long serialVersionUID = 1L;

    public ClockIn() {
    }

    public ClockIn(String id, Date date, Date startTime, Date endTime, Date actualTime, String status, String retroactive, String uId) {
        this.id = id;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.actualTime = actualTime;
        this.status = status;
        this.retroactive = retroactive;
        this.uId = uId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getActualTime() {
        return actualTime;
    }

    public void setActualTime(Date actualTime) {
        this.actualTime = actualTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public String getRetroactive() {
        return retroactive;
    }

    public void setRetroactive(String retroactive) {
        this.retroactive = retroactive == null ? null : retroactive.trim();
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId == null ? null : uId.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", date=").append(date);
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", actualTime=").append(actualTime);
        sb.append(", status=").append(status);
        sb.append(", retroactive=").append(retroactive);
        sb.append(", uId=").append(uId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}
