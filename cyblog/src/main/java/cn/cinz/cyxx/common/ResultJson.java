package cn.cinz.cyxx.common;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModelProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * 公共JSON格式类
 *
 * @Author: zengcheng
 * @Description: 返回公共JSON信息
 * @Date: 2021/9/4 15:23
 */
public class ResultJson {

    @ApiModelProperty(value = "是否成功")
    private Boolean success;

    /**
     * 响应编码
     * 100-请求成功
     * 101-请求异常
     * 103-未登录
     * 104-请求失败
     */
    @ApiModelProperty(value = "返回码")
    private Integer code;

    @ApiModelProperty(value = "返回消息")
    private String message;

    @ApiModelProperty(value = "token令牌")
    private String token;

    @ApiModelProperty(value = "范围 (例如1-10条数据)")
    private Integer limit;

    @ApiModelProperty(value = "页码")
    private Integer page;

    @ApiModelProperty(value = "数据总条数")
    private Integer count;

    @ApiModelProperty(value = "返回数据")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")//避免从数据库取日期的时候 变成时间戳
    private Map<String, Object> data = new HashMap<String, Object>();

    private ResultJson() {
    }

    /**
     * 成功时调用这个方法
     * @return
     */
    public static ResultJson ok() {
        ResultJson r = new ResultJson();
        r.setSuccess(true);
        r.setCode(ResultCode.SUCCESS);
        r.setMessage("成功");
        return r;
    }

    /**
     * 失败时调用这个方法
     * @return
     */
    public static ResultJson error() {
        ResultJson r = new ResultJson();
        r.setSuccess(false);
        r.setCode(ResultCode.ERROR);
        r.setMessage("失败");
        return r;
    }

    /**
     * token令牌
     * @param token
     * @return
     */
    public ResultJson token(String token){
        this.setToken(token);
        return this;
    }

    public ResultJson success(Boolean success) {
        this.setSuccess(success);
        return this;
    }

    /**
     * 消息
     * @param message
     * @return
     */
    public ResultJson message(String message) {
        this.setMessage(message);
        return this;
    }

    /**
     * 状态码
     * @param code
     * @return
     */
    public ResultJson code(Integer code) {
        this.setCode(code);
        return this;
    }

    /**
     * 截取 x - y
     * @param limit
     * @return
     */
    public ResultJson limit(Integer limit) {
        this.setLimit(limit);
        return this;
    }

    /**
     * 页码
     * @param page
     * @return
     */
    public ResultJson page(Integer page) {
        this.setPage(page);
        return this;
    }

    /**
     * 数据总条数
     * @param count
     * @return
     */
    public ResultJson count(Integer count) {
        this.setCount(count);
        return this;
    }

    public ResultJson data(Map<String, Object> map) {
        this.setData(map);
        return this;
    }

    /**
     * 数据
     * @param value
     * @return
     */
    public ResultJson data(Object value) {
        this.data.put("items", value);
        return this;
    }



    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
