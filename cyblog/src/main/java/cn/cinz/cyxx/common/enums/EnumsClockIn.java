package cn.cinz.cyxx.common.enums;

/**
 * @Description 字典枚举
 * @Author zengcheng
 * @Date 2021/10/22 9:30
 * @Version 1.0
 */
public enum EnumsClockIn {

    未打卡(0, "未打卡"),
    已打卡(1, "已打卡"),
    已补签(2, "已补签"),
    未补签(3, "未补签"),
    ;

    private Integer value;
    private String name;

    EnumsClockIn(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
