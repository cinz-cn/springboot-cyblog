package cn.cinz.cyxx.controller;

import cn.cinz.cyxx.common.enums.ELoginStatus;
import cn.cinz.cyxx.common.enums.ESendSms;
import cn.cinz.cyxx.common.ResultJson;
import cn.cinz.cyxx.model.User;
import cn.cinz.cyxx.service.BlogService;
import cn.cinz.cyxx.service.LoginService;
import cn.cinz.cyxx.util.IdUtils;
import cn.cinz.cyxx.util.RedisVerifyPhoneCodeUtils;
import cn.cinz.cyxx.util.StringUtils;
import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.Duration;

/**
*
* @ClassName LoginController
* @Description 登陆验证
* @Author zengcheng
* @Date 2021/10/2 18:45
* @Version 1.0
*/
@Controller
@CrossOrigin
public class LoginController {

    @Autowired
    private BlogService blogService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 日志
     */
    private Logger logger = LoggerFactory.getLogger(LoginController.class);

    /**
     * 登陆校验
     * @param user
     * @param code
     * @param session
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/loginVerification",method = RequestMethod.POST)
    public ResultJson loginVerification(User user, String code, HttpSession session, HttpServletRequest request,HttpServletResponse response){

        //if (StringUtils.isNotBlank(code)){

            String flag = RedisVerifyPhoneCodeUtils.checkVerifyCode(user.getPhone(),code);

            //if (flag == ESendSms.验证码正确.getName()){
                User userInfo = loginService.insertUser(user);
                if (StringUtils.isNotBlank(userInfo)){

                    session.setAttribute("user",userInfo);
//                    //生成token令牌
//                    String token  = IdUtils.getId();
//                    //存到Redis数据库
//                    //token为key userinfo 为value 30L为存放时间
//                    redisTemplate.opsForValue().set(token,userInfo, Duration.ofMinutes(30L));

                    return ResultJson.ok().message(ELoginStatus.登陆成功.getName()).code(100);
                }else {
                    return ResultJson.error().message(ELoginStatus.登陆失败.getName());
                }
//            }else {
//                return ResultJson.error().message(ESendSms.验证码错误.getName());
//            }

//        }else{
//            return ResultJson.error().message(ESendSms.验证码不能为空.getName());
//        }



    }

    @ResponseBody
    @RequestMapping(value = "/getVerifyCode",method = RequestMethod.POST)
    public ResultJson getVerifyCode(String phone){

        String flag = checkVerifyCode(phone);
        if (flag == ESendSms.发送成功.getName()){
            return ResultJson.ok().message(flag);
        }else {
            return ResultJson.error().message(flag);
        }


    }


    //重定向到首页博客显示
    @RequestMapping(value = "/user/homeBlog/{id}",method = RequestMethod.GET)
    public String homeBlog(@PathVariable("id") String id, Model model){

        model.addAttribute("id",id);
        try {
            blogService.updateBlogPageViews(id);
            return "user/homeBlog";
        } catch (Exception e) {
            model.addAttribute("errorMsg","增加博客浏览量时出错");
            return "/error";
        }


    }


    /**
     * 校验验证码方法
     * @param phone
     * @return
     */
    String checkVerifyCode(String phone){

        String flag = RedisVerifyPhoneCodeUtils.getVerifyCode(phone);

        if (flag == ESendSms.发送成功.getName()){
            return ESendSms.发送成功.getName();
        }else {
            return ESendSms.一小时只可发送三条.getName();
        }

    }


}
