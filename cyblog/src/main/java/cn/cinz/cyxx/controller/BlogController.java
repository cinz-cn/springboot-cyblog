package cn.cinz.cyxx.controller;

import cn.cinz.cyxx.common.enums.EBlog;
import cn.cinz.cyxx.common.ResultJson;
import cn.cinz.cyxx.model.*;
import cn.cinz.cyxx.service.BlogService;
import cn.cinz.cyxx.util.IdUtils;
import cn.cinz.cyxx.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * 博客控制器
 *
 * @Author: zengcheng
 * @Description: 博客的所有处理
 * @Date: 2021/9/11 22:50
 */
@Controller
@CrossOrigin
@RequestMapping("/blog")
public class BlogController {

    /**
     * 日志
     */
    private Logger logger = LoggerFactory.getLogger(BlogController.class);

    @Autowired
    private BlogService blogService;

    @ResponseBody
    @RequestMapping(value = "/listData",method = RequestMethod.GET)
    public ResultJson selectBlogList(BlogQuery blogQuery){


        blogQuery.setStatus(EBlog.公开.getValue());
        List<BlogAdd> list = null;
        try {
            list = blogService.selectBlogList(blogQuery);
            if (list != null && list.size() > 0){
                return ResultJson.ok().data(list).count(list.size());
            }else {
                return ResultJson.error().message("未找到");
            }
        } catch (Exception e) {
            return ResultJson.error().message(e.getMessage());
        }

    }



    @ResponseBody
    @RequestMapping(value = "/listData/{id}",method = RequestMethod.POST)
    public ResultJson selectBlogId(@PathVariable("id") String id){

        Blog blog = new Blog();

        blog.setId(id);
        blog.setStatus(EBlog.公开.getValue());

        List<BlogAdd> list = null;
        try {
            list = blogService.selectBlogList(blog);
            return ResultJson.ok().data(list);
        } catch (Exception e) {
            return ResultJson.error().message(e.getMessage());
        }


    }

    @ResponseBody
    @RequestMapping(value = "/insertBlog", method = RequestMethod.POST)
    public ResultJson insertBlog(Blog blog, HttpSession session){

        if (blog.getValue() == null || blog.getValue() == ""){
            return ResultJson.error().message("请填写博客内容");
        }


        User userInfo = (User) session.getAttribute("user");
        blog.setId(IdUtils.getId());
        blog.setCreateDate(new Date());
        blog.setuId(userInfo.getId());
        logger.info(String.valueOf(blog));
        try {
            blogService.insertBlog(blog);
        } catch (Exception e) {
            return ResultJson.error().message(e.getMessage());
        }

        return ResultJson.ok();
    }

    /**
     * 管理我的博客
     * @return
     */
    @RequestMapping("/myBlogList")
    public String myBlogList(){
        return "user/blog/my_blog_list";
    }

    /**
     * 我的博客数据列表
     * @param session
     * @param blogQuery
     * @return
     */
    @ResponseBody
    @GetMapping("/myBlogList/listData")
    public ResultJson myBlogListData(HttpSession session,BlogQuery blogQuery){

        blogQuery.setPage((blogQuery.getPage() * blogQuery.getLimit()) - blogQuery.getLimit());
        blogQuery.setLimit(blogQuery.getLimit());

        User user = (User) session.getAttribute("user");

        MyBlog myBlog = new MyBlog();
        myBlog.setBlogQuery(blogQuery);
        myBlog.setUser(user);

        try {
            List<Blog> blogList = blogService.selectMyBlogList(myBlog);
            Integer count = blogService.countBlog(user.getId());

            return ResultJson.ok().data(blogList).count(count);
        } catch (Exception e) {
            return ResultJson.error().message(e.getMessage());
        }

    }

    /**
     * 删除一篇博客
     * @param blog
     * @return
     */
    @ResponseBody
    @RequestMapping("/deleteBlog")
    public ResultJson deleteBlog(Blog blog){
        try {
            blogService.deleteBlog(blog);
            return ResultJson.ok();
        } catch (Exception e) {
            return ResultJson.error();
        }
    }

    /**
     * 博客修改页面跳转
     * @return
     */
    @RequestMapping("/edit/{id}")
    public String editBlog(@PathVariable("id") String id, Model model){
        model.addAttribute("id",id);
        return "user/blog/my_blog_update";
    }

    /**
     * 博客修改页面数据
     * @param blog
     * @return
     */
    @RequestMapping("/edit/data")
    @ResponseBody
    public ResultJson editBlogData(Blog blog){
        try {
            blog = blogService.selectBlog(blog);
            if (blog != null){
                return ResultJson.ok().data(blog);
            }else{
                return ResultJson.error().message("未找到此博客内容");
            }
        } catch (Exception e) {
            return ResultJson.error().message(e.getMessage());
        }
    }

    /**
     * 博客修改保存
     * @param blog
     * @return
     */
    @RequestMapping(value =  "/edit/save", method = RequestMethod.POST)
    @ResponseBody
    public ResultJson editSave(Blog blog){

        if (blog.getValue() == null && blog.getValue() == ""){
            return ResultJson.error().message("博客内容不能为空");
        }
        try {
            blogService.updateByPrimaryKey(blog);
            return ResultJson.ok().message("修改成功");
        } catch (Exception e) {
            return ResultJson.error().message(e.getMessage());
        }

    }

}
