package cn.cinz.cyxx.controller.clock_in;

import cn.cinz.cyxx.common.ResultJson;
import cn.cinz.cyxx.model.clock_in.ClockIn;
import cn.cinz.cyxx.model.clock_in.ClockInExample;
import cn.cinz.cyxx.service.clock_in.ClockInService;
import cn.cinz.cyxx.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;

/**
 * @Description 打卡控制类
 * @Author zengcheng
 * @Date 2021/10/22 10:30
 * @Version 1.0
 */

@Controller
@CrossOrigin
@RequestMapping("/clock_in")
public class ClockInController {

    private Logger logger = LoggerFactory.getLogger(ClockInController.class);

    @Autowired
    private ClockInService clockInService;

    /**
     * 打卡页跳转
     * @return
     */
    @RequestMapping("")
    public String clockIn(){
        return "user/clock_in/clock_in";
    }

    /**
     * 显示打卡数据
     * @param clockIn
     * @return
     */
    @RequestMapping("/showData")
    @ResponseBody
    public ResultJson showDate(ClockIn clockIn){
        try {
            List<ClockIn> clockInList= clockInService.selectClockIn(clockIn);
            if (clockInList.size() > 0){
                return ResultJson.ok().data(clockInList);
            }else{
                return ResultJson.error().message("没有找到你的打卡信息");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResultJson.error().message(e.getMessage());
        }
    }

}
