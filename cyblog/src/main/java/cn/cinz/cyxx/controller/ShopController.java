package cn.cinz.cyxx.controller;

import cn.cinz.cyxx.common.ResultJson;
import cn.cinz.cyxx.model.PayModel;
import cn.cinz.cyxx.util.PayUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
* 商城控制器
* @ClassName ShopController
* @Description 商城控制器
* @Author zengcheng
* @Date 2021/10/5 9:30
* @Version 1.0
*/
@CrossOrigin
@Controller
@RequestMapping("/user/shop")
public class ShopController {

    /**
     * 日志
     */
    private Logger logger = LoggerFactory.getLogger(ShopController.class);

    /**
     * 订单支付
     * @return
     */
    @RequestMapping("/payment")
    public String index(){
        return "user/shop/payment";
    }

    @RequestMapping("/pay")
    @ResponseBody
    public ResultJson pay(HttpServletRequest request, float money, int pay_type) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        Map<String, Object> remoteMap = new HashMap<String, Object>();
        remoteMap.put("money", money);
        remoteMap.put("pay_type", pay_type);
        remoteMap.put("order_no", PayUtil.getOrderIdByUUId());
        remoteMap.put("subject", "商品名称");
        remoteMap.put("app_id", PayUtil.APP_ID);
        remoteMap.put("extra", "");
        resultMap.put("data", PayUtil.payOrder(remoteMap));
        return ResultJson.ok().data(resultMap);
    }


    /**
     * 支付成功，支付接口自动回调
     * @param request
     * @param response
     * @param payModel
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/notifyPay", method = RequestMethod.POST)
    @ResponseBody
    public String notifyPay(HttpServletRequest request, HttpServletResponse response, PayModel payModel) throws IOException {
        // 保证密钥一致性
        if (PayUtil.checkSign(payModel)) {
            // TODO 支付成功的业务处理
            //这里业务处理
            //如果支付金额是前端传递过来的，这里还要验证支付金额
            logger.info("success");
            return "success";
        } else {
            // 签名错误
            logger.info("签名错误");
            response.getWriter().println("签名错误");
            return "error";
        }
    }

    @RequestMapping(value = "/returnPay", method = RequestMethod.GET)
    public String returnPay(HttpServletRequest request, HttpServletResponse response, PayModel payModel) throws IOException {

        // 保证密钥一致性
        if (PayUtil.checkSign(payModel)) {
            // 建议本面仅显示支付结果,支付成功的业务处理放在如上的notifyPay里
            response.getWriter().println("恭喜支付成功！订单号：" + payModel.getOrder_no());
            logger.info("恭喜支付成功！订单号：" + payModel.getOrder_no());
            logger.info("success");
            response.getWriter().println("success");
        } else {
            // 签名错误
            logger.info("签名错误");
            response.getWriter().println("签名错误");
        }
        return "redirect:/user/shop/payOk";
    }


    /**
     * 跳转至支付成功页面
     * @return
     */
    @RequestMapping(value = "/payOk", method = RequestMethod.GET)
    public String payOK(){

        return "user/shop/payOk";
    }

    /**
     * 订单列表
     * @return
     */
    @RequestMapping(value = "/orderList", method = RequestMethod.GET)
    public String orderList(){

        return "user/shop/orderList";
    }


}
