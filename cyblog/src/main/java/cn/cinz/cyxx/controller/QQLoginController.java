package cn.cinz.cyxx.controller;

import cn.cinz.cyxx.common.enums.EUser;
import cn.cinz.cyxx.model.User;
import cn.cinz.cyxx.service.LoginService;
import com.qq.connect.QQConnectException;
import com.qq.connect.api.OpenID;
import com.qq.connect.api.qzone.UserInfo;
import com.qq.connect.javabeans.AccessToken;
import com.qq.connect.javabeans.qzone.UserInfoBean;
import com.qq.connect.oauth.Oauth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

/**
 * qq登陆
 *
 * @Author: zengcheng
 * @Description: qq登陆
 * @Date: 2021/9/2 20:08
 */
@Controller
@CrossOrigin
public class QQLoginController {

    @Autowired
    private LoginService loginService;

    /**
     * 请求QQ登录
     */
    @RequestMapping("/qqLogin")
    public void loginByQQ(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html;charset=utf-8");
        try {
            response.sendRedirect(new Oauth().getAuthorizeURL(request));
            System.out.println("请求QQ登录,开始跳转...");
        } catch (QQConnectException | IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * QQ登录回调地址
     *
     * @return
     */
    @RequestMapping("/connection")
    public String connection(HttpServletRequest request, HttpServletResponse response, Map<String,Object> map, HttpSession session, User user) {
        try {
            AccessToken accessTokenObj = (new Oauth()).getAccessTokenByRequest(request);
            String accessToken = null, openID = null;
            long tokenExpireIn = 0L;
            if ("".equals(accessTokenObj.getAccessToken())) {
            //                我们的网站被CSRF攻击了或者用户取消了授权
            //                做一些数据统计工作
                System.out.println("登录失败:没有获取到响应参数");
                return "accessTokenObj=>" + accessTokenObj + "; accessToken" + accessTokenObj.getAccessToken();
            } else {
                accessToken = accessTokenObj.getAccessToken();
                tokenExpireIn = accessTokenObj.getExpireIn();
                System.out.println("accessToken" + accessToken);
                request.getSession().setAttribute("demo_access_token", accessToken);
                request.getSession().setAttribute("demo_token_expirein", String.valueOf(tokenExpireIn));

                // 利用获取到的accessToken 去获取当前用的openid -------- start
                OpenID openIDObj = new OpenID(accessToken);
                openID = openIDObj.getUserOpenID();

                UserInfo qzoneUserInfo = new UserInfo(accessToken, openID);
                UserInfoBean userInfoBean = qzoneUserInfo.getUserInfo();
                if (userInfoBean.getRet() == 0) {
                    String name = removeNonBmpUnicode(userInfoBean.getNickname());
                    String sex = userInfoBean.getGender();

                    String imgUrl = userInfoBean.getAvatar().getAvatarURL50();
                    map.put("openId",openID);//qqid 唯一值
                    map.put("name",name);//qq用户名
                    map.put("sex",sex);//qq性别
                    map.put("imgUrl",imgUrl);//qq空间头像

                    user = new User(openID,name,imgUrl,sex,new Date(), EUser.普通用户.getValue());


                    User userInfo = loginService.insertQQUser(user);

                    session.setAttribute("user",userInfo);

                } else {
                    System.out.println("很抱歉，我们没能正确获取到您的信息，原因是：" + userInfoBean.getMsg());
                }
            }
        } catch (QQConnectException e) {
            e.printStackTrace();
        }
        return "qq";
    }

    /**
     * 处理掉QQ网名中的特殊表情
     * @param str
     * @return
     */
    public String removeNonBmpUnicode(String str) {
        if (str == null) {
            return null;
        }
        str = str.replaceAll("[^\\u0000-\\uFFFF]", "");
        if ("".equals(str)) {
            str = "(* _ *)";
        }
        return str;
    }

//    @RequestMapping("/qqInsert")
//    public String qqInsert(HttpSession session){
//
//        User user = (User) session.getAttribute("qqUserInfo");
//        User userInfo = loginService.insertQQUser(user);
//
//        if (userInfo.getAuthority() == EUser.普通用户.getValue()){
//            session.setAttribute("user",userInfo);
//            return "user/home";
//        }else if (userInfo.getAuthority() == EUser.管理员.getValue()){
//            session.setAttribute("user",userInfo);
//            return "admin/index";
//        }else {
//            session.setAttribute("user",userInfo);
//            return "admin/index";
//        }
//
//    }

}
