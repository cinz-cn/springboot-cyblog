package cn.cinz.cyxx.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
* 全局视图控制类
* @ClassName ViewController
* @Description 公共视图控制器
* @Author zengcheng
* @Date 2021/10/2 18:38 
* @Version 1.0
*/
@Controller
@CrossOrigin
public class ViewController {

    /**
     * 日志
     */
    private Logger logger = LoggerFactory.getLogger(ViewController.class);

    @RequestMapping("/")
    public String test(){

        return "index";
    }

    @RequestMapping("/login")
    public String login(){

        return "login";
    }

    @RequestMapping( "/qq")
    public String qq(){

        return "qq";
    }


    //重定向到首页
    @GetMapping("/user/home")
    public String userHome(){
        return "user/home";
    }

    @RequestMapping("/admin/index")
    public String adminHome(){


        return "admin/index";
    }

    //重定向到首页博客列表
    @RequestMapping("/user/homeBlogList")
    public String homeBlogList(){
        return "user/homeBlogList";
    }

    //重定向到首页博客列表  搜索标题
    @RequestMapping("/user/homeTitleList")
    public String homeTitleList(){
        return "user/homeTitleList";
    }


    //重定向到博客
    @RequestMapping(value = "/user/blog", method = RequestMethod.GET)
    public String blog(){
        return "user/blog";
    }

    //重定向到商城
    @RequestMapping("/user/shop")
    public String shop(){
        return "user/shop";
    }

    //重定向到测试域
    @RequestMapping("/user/testDomain")
    public String testDomain(){
        return "user/testDomain";
    }

    //重定向到测试域 demo
    @RequestMapping("/testDomain/MapDemo")
    public String testDomainDemo(){
        return "testDomain/MapDemo";
    }

    //重定向到等级体系
    @RequestMapping("/user/grade_system")
    public String gradeSystem(){
        return "user/grade_system";
    }

    //重定向到个人中心
    @RequestMapping("/user/myzoe")
    public String myzoe(){
        return "user/myzoe";
    }

    @RequestMapping("/user/index")
    public String pay(){
        return "user/index";
    }

    @RequestMapping( "/home/console")
    public String console(){
        return "admin/home/console";
    }
    

}
