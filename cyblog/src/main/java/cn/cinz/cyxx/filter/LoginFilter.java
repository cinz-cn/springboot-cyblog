//package cn.cinz.cyxx.filter;
//
//import cn.cinz.cyxx.common.ResultJson;
//import com.alibaba.fastjson.JSONObject;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.redis.core.RedisTemplate;
//
//import javax.servlet.*;
//import javax.servlet.annotation.WebFilter;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.util.concurrent.TimeUnit;
//
///**
//*
//* @Description: TOO
//* @Author zengcheng
//* @Date 2021/10/7 10:09
//* @Version 1.0
//*/
//@Configuration
//@WebFilter(filterName = "LoginFilter")
//public class LoginFilter implements Filter {
//
//    /**
//     * 日志
//     */
//    private Logger logger = LoggerFactory.getLogger(LoginFilter.class);
//
//
//    @Autowired
//    private RedisTemplate<String, Object> redisTemplate;
//
//    @Override
//    public void init(FilterConfig config) throws ServletException {
//    }
//
//    @Override
//    public void destroy() {
//    }
//
//    @Override
//    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws ServletException, IOException {
//
//        HttpServletRequest request = (HttpServletRequest) servletRequest;
//        HttpServletResponse response = (HttpServletResponse) servletResponse;
//
//        String origin = request.getHeader("Origin");
//        response.addHeader("Access-Control-Allow-Origin",origin);
//        response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
//        response.addHeader("Access-Control-Max-Age","3600");
//        response.addHeader("Content-Type", "application/x-www-form-urlencoded");
//        response.addHeader("Access-Control-Allow-Headers", "Content-Type,X-Requested-With,accept,Origin,Access-Control-Request-Method,Access-Control-Request-Headers,token");
//        response.addHeader("Access-Control-Allow-Credentials","true");
//
//        String url = request.getRequestURI();
//        logger.info("过滤器--->(目前请求路径):"+url);
//
//        if (url.indexOf("/")>=0 || url.indexOf("/login")>=0 || url.indexOf("qqLogin")>=0 || url.indexOf("/static/**")>=0 || url.indexOf("loginVerification")>=0){
//            chain.doFilter(servletRequest,servletResponse);
//        }
//
//        //获取Headers中的token参数
//        String token = request.getHeader("token");
//        token = token == null ? "" : token;
//
//        //查询Redis中是否存在  查询key在Redis中的剩余时间
//        Long expire = redisTemplate.getExpire(token);
//
//        if (expire>0){
//
//            redisTemplate.expire(token,30L, TimeUnit.MINUTES);
//
//            chain.doFilter(servletRequest,servletResponse);
//        }else{
//
//            String info = JSONObject.toJSONString(ResultJson.error().message("未登录").code(103));
//
//            response.setContentType("json/text;charset=utf-8");
//            PrintWriter out = response.getWriter();
//
//            out.write(info);
//
//        }
//
//    }
//
//}
