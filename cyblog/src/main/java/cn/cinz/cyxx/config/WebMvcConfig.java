//package cn.cinz.cyxx.config;
//
//import cn.cinz.cyxx.interceptor.LoginInterceptor;
//import cn.cinz.cyxx.util.SpringUtil;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.web.servlet.HandlerInterceptor;
//import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//@Configuration
//public class WebMvcConfig extends WebMvcConfigurationSupport {
//
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//
//        /**
//         * 解决跨域问题
//         */
//        registry.addInterceptor(new HandlerInterceptor(){
//            @Override
//            public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//                //response.addHeader("Access-Control-Allow-Origin", "*");
//                response.addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
//                response.addHeader("Content-Type", "application/x-www-form-urlencoded");
//                response.addHeader("Access-Control-Allow-Headers", "Content-Type,X-Requested-With,accept,Origin,Access-Control-Request-Method,Access-Control-Request-Headers,token");
//                return true;
//            }
//        });
//
//        /**
//         * //拦截所有请求
//         * //放行资源
//         */
//        registry.addInterceptor(new LoginInterceptor())
//                .addPathPatterns("/**")
//                .excludePathPatterns("/","/static/**","/login","/qqLogin","/connection","/loginVerification","/getVerifyCode","/favicon.ico","/user/shop/notifyPay")
//        ;
//
//        registry.addInterceptor((HandlerInterceptor) SpringUtil.getBean("loginInterceptor"));
//        super.addInterceptors(registry);
//    }
//}