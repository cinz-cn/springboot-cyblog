package cn.cinz.cyxx.mapper;

import cn.cinz.cyxx.model.*;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * (描述功能模块)
 *
 * @Author: zengcheng
 * @Description: (内容作用)
 * @Date: 2021/9/11 22:22
 */
@Repository
public interface BlogMapper{

    /**
     * 查找所有公开博客
     * @param blog
     * @return
     */
    List<BlogAdd> selectBlogList(Blog blog) throws Exception;

    /**
     * 查找一篇博客
     * @param blog
     * @return
     */
    Blog selectBlog(Blog blog) throws Exception;

    /**
     * 统计博客条数 用于分页
     * @return
     */
    Integer countBlog(String id);

    /**
     * 查找我的所以博客
     * @param myBlog
     * @return
     * @throws Exception
     */
    List<Blog> selectMyBlogList(MyBlog myBlog) throws Exception;

    /**
     * 通过博客id修改博客内容
     * @param blog
     * @return
     */
    int updateByPrimaryKey(Blog blog) throws Exception;

    /**
     * 通过博客id修改浏览量   浏览量=浏览量+1
     * @param id
     */
    void updateBlogPageViews(String id) throws Exception;

    /**
     * 删除一篇博客
     * @param blog
     * @return
     */
    int deleteBlog(Blog blog) throws Exception;

    /**
     * 插入博客
     * @param blog
     * @return
     */
    int insertBlog(Blog blog) throws Exception;
}
