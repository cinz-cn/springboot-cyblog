package cn.cinz.cyxx.mapper;

import cn.cinz.cyxx.model.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
* 用户Mapper
* @ClassName UserMapper
* @Description 用户Mapper
* @Author zengcheng
* @Date 2021/10/4 10:24
* @Version 1.0
*/
@Repository
public interface UserMapper{

    /**
     * 根据用户id获取用户信息
     * @param id
     * @return
     */
    User getUserinfo(@Param("id") String id) throws Exception ;

    /**
     * 修改用户信息
     * @param user
     * @return
     */
    void editUserInfo(User user) throws Exception;

}
