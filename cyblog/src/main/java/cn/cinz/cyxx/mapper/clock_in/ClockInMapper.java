package cn.cinz.cyxx.mapper.clock_in;

import cn.cinz.cyxx.model.clock_in.ClockIn;
import cn.cinz.cyxx.model.clock_in.ClockInExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
*
*  @Description
*  @Author zengcheng
*  @Date 2021/10/22 10:04
*  @Version 1.0
*/

@Repository
public interface ClockInMapper{

    /**
     * 查找一条 打卡信息
     * @param clockIn
     * @return
     */
    List<ClockIn> selectClockIn(ClockIn clockIn) throws Exception;

    int countByExample(ClockInExample example) throws Exception;

    int deleteByExample(ClockInExample example) throws Exception;

    int deleteByPrimaryKey(String id) throws Exception;

    int insert(ClockIn record) throws Exception;

    int insertSelective(ClockIn record) throws Exception;

    List<ClockIn> selectByExample(ClockInExample example) throws Exception;

    ClockIn selectByPrimaryKey(String id) throws Exception;

    int updateByExampleSelective(@Param("record") ClockIn record, @Param("example") ClockInExample example) throws Exception;

    int updateByExample(@Param("record") ClockIn record, @Param("example") ClockInExample example) throws Exception;

    int updateByPrimaryKeySelective(ClockIn record) throws Exception;

    int updateByPrimaryKey(ClockIn record) throws Exception;

}
