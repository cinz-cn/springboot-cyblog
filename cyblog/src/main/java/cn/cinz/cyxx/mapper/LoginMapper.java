package cn.cinz.cyxx.mapper;

import cn.cinz.cyxx.model.User;
import org.springframework.stereotype.Repository;

/**
 * (描述功能模块)
 *
 * @Author: zengcheng
 * @Description: (内容作用)
 * @Date: 2021/9/6 23:18
 */
@Repository
public interface LoginMapper {

    /**
     * 注册
     * @param user
     * @return
     */
    User insertUser(User user);

    User insertQQUser(User user);

    User selectUser(User user);

    User selectQQUser(String id);

}
