package cn.cinz;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zengcheng
 */
@SpringBootApplication
@MapperScan({"cn.cinz.cyxx.mapper","cn.cinz.cyxx.mapper.*"})
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
