var vueDate;
vueDate = new Vue({
    el:'#myzoeInfo',
    data:{
        update: {
            id: '',
            name: '',
            sex: '',
            img: '',
            phone: '',
        },
        items: {
        },
    },
    created:function(){//mounted 异步请求都写在这里  在这个阶段，数据和DOM都已被渲染出来。
        this.showData();
    },
    methods:{
        showData:function (){
            $.ajax({
                url: '/user/getUserInfo',
                method: 'POST',
                success: function(data){
                    if (data.success){
                        vueDate.items = data.data.items
                    }else{
                       alert(data.message)
                    }
                }
            })
        },
        editName: function (obj){
            //修改个人信息
            if (obj === null || obj === ''){
                alert("请填写内容后再修改")
            }else{
                $.ajax({
                    url: '/user/editUserInfo',
                    data: {
                        "id": vueDate.items.id,
                        "name": vueDate.items.name,
                    },
                    method: 'GET',
                    success: function (data){
                        if (data.success){
                            alert(data.message);
                            vueDate.showData();
                        }else{
                            alert(data.message);
                        }
                    }
                })
            }
        },
        editSex: function (obj){
            //修改个人信息
            if (obj === null || obj === ''){
                alert("请填写内容后再修改")
            }else{
                $.ajax({
                    url: '/user/editUserInfo',
                    data: {
                        "id": vueDate.items.id,
                        "sex": obj,
                    },
                    method: 'GET',
                    success: function (data){
                        if (data.success){
                            alert(data.message);
                            vueDate.showData();
                        }else{
                            alert(data.message);
                        }
                    }
                })
            }
        },
    },

})