var vueDate = null;
    vueDate = new Vue({
        el: '#myBlogList',
        data: {
            items: {
            },
            count: '',
        },
        created:function(){
            this.myBlogListInfo();
        },
        methods: {
            myBlogListInfo:function (){
                layui.use('table', function() {
                    var table = layui.table;
                    //第一个实例
                    table.render({
                        toolbar: '#toolbarDemo', //开启头部工具栏，并为其绑定右侧模板
                        defaultToolbar: [ 'exports', 'print'],
                        elem: '#demo',
                        url: '/blog/myBlogList/listData',//数据接口
                        page: true, //开启分页
                        title: '我的博客',
                        //even: true, //开启隔行背景
                        size: 'sm', //小尺寸的表格
                        cols: [
                            [ //表头
                            {
                                fixed: 'left',
                                type:'checkbox'
                            },
                            {
                                field: 'id',
                                title: 'id',
                                hide: true //隐藏字段
                            },
                            {
                                field: 'title',
                                title: '标题'
                            }, {
                                field: 'createDate',
                                title: '创建时间',
                                sort: true
                            }, {
                                field: 'memo',
                                title: '签名'
                            }, {
                                field: 'value',
                                title: '内容'
                            }, {
                                field: 'status',
                                title: '状态'
                            }, {
                                field: 'pageViews',
                                title: '浏览量',
                                sort: true
                            }, {
                                fixed: 'right',
                                title: '操作',
                                toolbar: '#barDemo',
                                width: 120
                            }
                            ]
                        ],
                        done:function (res, curr, count){
                            //数据显示转换
                            $("[data-field='status']").children().each(function (){
                                  if ($(this).text() === 'true'){
                                      $(this).text('公开')
                                  }else if ($(this).text() === 'false'){
                                      $(this).text('私密')
                                  }
                            });
                        },
                        text: {
                            none: '暂无相关数据' //自定义文本，如空数据时的异常提示等。
                        },
                        parseData: function(data) {
                            return {
                                "code": data.code, //解析接口状态
                                "msg": data.message, //解析提示文本
                                "count": data.count, //解析数据长度
                                "data": data.data.items //解析数据列表
                            }
                        }
                    });
                });
                layui.use('table', function() {

                    var table = layui.table;
                    //监听工具条
                    table.on('tool(test)', function(obj) {
                        var data = obj.data;
                        if (obj.event === 'del') {
                            layer.confirm('真的删除行么', function(index) {
                                //**执行删除操作**
                                $.post("/blog/deleteBlog", {
                                    "id": data.id
                                }, function() {
                                    layer.alert('删除成功');
                                    //删除后重载数据
                                    table.reload('demo',{
                                        url: '/blog/myBlogList/listData'
                                    });
                                })
                            });
                        } else if (obj.event === 'edit') {
                            //跳转至博客修改界面
                            window.open("/blog/edit/"+data.id,'_self');
                        }


                    });
                });
            },
            editBlog:function (id){
                console.log("edit")
            },
            deleteBlog:function (id){
                console.log("delete")
            },
        },
    })


