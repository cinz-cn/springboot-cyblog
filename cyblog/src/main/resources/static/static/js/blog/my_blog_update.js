var Main = {
    data() {
        return {
            ruleForm: {
                title: '',
                memo: [''],
                status: true, //true 公开  false 私密
                value: '',
            },
            rules: {
                title: [
                    { required: true, message: '请输入标题', trigger: 'blur' },
                    { min: 3, max: 50, message: '长度在 3 个字符以上', trigger: 'blur' }
                ],
                status: [
                    { required: true, message: '请选择博客状态', trigger: 'blur' }
                ],
                memo: [
                    { required: true, message: '请选择博客标签', trigger: 'change' }
                ],
            }
        };
    },
    created:function (){
        this.editData()
    },
    methods: {
        editData:function (){
            let _this = this;
            $.ajax({
                url: '/blog/edit/data',
                data:{
                    "id": $("#id").val()
                },
                success:function (data){
                    if (data.success){
                        _this.ruleForm = data.data.items
                    }else{
                        alert(data.message)
                    }
                }
            })
        },
        submitForm(formName) {
            let _this = this;
            this.$refs[formName].validate((valid) => {
                if (valid) {
                    if (editor.txt.html() == null || editor.txt.html() == ''){//判断富文本是否有值
                        alert('请填写博客内容')
                        return false
                    }else {
                        $.ajax({
                            url: '/blog/edit/save',
                            data: {
                                'id': $("#id").val(),
                                'title': this.ruleForm.title,
                                'memo': this.ruleForm.memo,
                                'status': this.ruleForm.status,
                                'value': editor.txt.html()//获取富文本中的内容
                            },
                            type: 'POST',
                            success:function (data) {
                                if (data.success){
                                    alert(data.message+'修改')
                                }else {
                                    alert(data.message)
                                }
                            }
                        })
                    }
                } else {
                    return false;
                }
            });
        },
        resetForm(formName) {
            editor.txt.clear();//清空富文本编辑器内容
            this.$refs[formName].resetFields();
        }
    }
}
var Ctor = Vue.extend(Main)
new Ctor().$mount('#blogEdit')

var E = window.wangEditor;
var editor = new E("#value");
// 挂载highlight插件
editor.highlight = hljs
editor.config.languageTab = '    ' //对tab键进行处理
editor.config.showFullScreen = true;
editor.config.zIndex = 10
// 默认情况下，显示所有菜单   菜单配置
editor.config.menus = [
    'head',
    'bold',
    'fontSize',
    'fontName',
    'italic',
    'underline',
    'strikeThrough',
    'indent',
    'lineHeight',
    'foreColor',
    'backColor',
    'link',
    'todo',
    'justify',
    'quote',
    'image',
    'video',
    'table',
    'code',
    'splitLine',
    'undo',
    'redo',
]
editor.create();


