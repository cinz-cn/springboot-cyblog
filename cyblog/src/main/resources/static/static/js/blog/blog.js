var Main = {
    data() {
        return {
            ruleForm: {
                title: '',
                region: '',
                //date1: '',
                //date2: '',
                status: true, //true 公开  false 私密
            },
            rules: {
                title: [
                    { required: true, message: '请输入标题', trigger: 'blur' },
                    { min: 3, max: 50, message: '长度在 3 个字符以上', trigger: 'blur' }
                ],
                status: [
                    { required: true, message: '请选择博客状态', trigger: 'blur' }
                ],
                region: [
                    { required: true, message: '请选择博客标签', trigger: 'change' }
                ],
                // date1: [
                //     { type: 'date', required: true, message: '请选择日期', trigger: 'change' }
                // ],
                // date2: [
                //     { type: 'date', required: true, message: '请选择时间', trigger: 'change' }
                // ],
            }
        };
    },
    methods: {
        submitForm(formName) {
            let _this = this;
            this.$refs[formName].validate((valid) => {
                if (valid) {

                    if (editor.txt.html() == null || editor.txt.html() == ''){//判断富文本是否有值
                        alert('请填写博客内容')
                        return false
                    }else {
                        $.ajax({
                            url: '/blog/insertBlog',
                            data: {
                                'title': this.ruleForm.title,
                                'memo': this.ruleForm.region,
                                'status': this.ruleForm.status,
                                'value': editor.txt.html()//获取富文本中的内容
                            },
                            type: 'POST',
                            success:function (data) {
                                if (data.success){
                                    alert(data.message+'发布')
                                    editor.txt.clear();//清空富文本编辑器内容
                                    _this.$refs[formName].resetFields();
                                }else {
                                    alert(data.message)
                                }
                            }
                        })
                    }
                    //alert('标题:'+this.ruleForm.title+",标签:"+this.ruleForm.region+",状态:"+this.ruleForm.status+",内容:"+this.ruleForm.value);//this.ruleForm获取表单信息

                } else {
                    return false;
                }
            });
        },
        showBlogList:function (){
            window.open("/blog/myBlogList",'_self');
        },
        resetForm(formName) {
            editor.txt.clear();//清空富文本编辑器内容
            this.$refs[formName].resetFields();
        }
    }
}
var Ctor = Vue.extend(Main)
new Ctor().$mount('#app')

var E = window.wangEditor;
var editor = new E("#value");
// 挂载highlight插件
editor.highlight = hljs
editor.config.languageTab = '    ' //对tab键进行处理
editor.config.showFullScreen = true;
editor.config.zIndex = 10
// 默认情况下，显示所有菜单   菜单配置
editor.config.menus = [
    'head',
    'bold',
    'fontSize',
    'fontName',
    'italic',
    'underline',
    'strikeThrough',
    'indent',
    'lineHeight',
    'foreColor',
    'backColor',
    'link',
    'list',
    'todo',
    'justify',
    'quote',
    'emoticon',
    'image',
    'video',
    'table',
    'code',
    'splitLine',
    'undo',
    'redo',
]
editor.create();


