
var Main = {
    data() {
        return {
            query: {
                phone: ''
            },
            ruleForm: {
                phone: '',
                code: ''
            },
            rules: {
                phone: [
                    { required: true, message: '请输入手机号', trigger: 'blur' },//不能为空
                    { min : 11 , max : 11 , message: '请输入11位手机号' , trigger: 'blur'},
                ],
                code: [
                    { required: true, message: '请填写验证码', trigger: 'blur' }
                ]
            }
        };
    },
    methods: {
        submitForm(formName) {
            let _this = this;
            this.$refs[formName].validate((valid) => {
            if (valid) {
                $.ajax({
                    url: '/loginVerification',
                    data:{
                            'phone': this.ruleForm.phone,
                            'code': this.ruleForm.code
                    },
                    method: 'POST',
                    success:function (data) {
                        if (data.success){//成功跳转至首页
                            _this.$message({
                                message: data.message,//弹窗内容
                                type: 'success',//成功消息样式
                                showClose: true,//关闭框
                                duration: 3000,
                            });

                            localStorage.setItem("token",data.token);

                            if(data.code === 100){
                                setTimeout("window.location.href = '/user/home'",1000)//提示登陆成功，等待一秒进行跳转
                            }


                        }else{
                            _this.$message({
                                message: data.message,//弹窗内容
                                type: 'error',//成功消息样式
                                showClose: true,//关闭框
                            });
                        }
                    }
                })

                return true;

            } else {
                    this.$message({
                        message: '请认真填写',//弹窗内容
                        type: 'error',//消息样式
                        showClose: true,//关闭框
                    });
                    return false;
                }
            });
        },
        smsCode:function () {
            let _this = this;
            $.ajax({
                url: '/getVerifyCode',
                method: 'POST',
                data: {
                    'phone': this.ruleForm.phone,
                },
                success:function (data){
                    if (data.success){
                        _this.$message({
                            message: data.message,//弹窗内容
                            type: 'success',//成功消息样式
                            showClose: true,//关闭框
                            duration: 3000,
                        });
                    }else {
                        _this.$message({
                            message: data.message,//弹窗内容
                            type: 'success',//成功消息样式
                            showClose: true,//关闭框
                            duration: 3000,
                        });
                    }
                }
            })
        },
    },
    mounted(){

    }
}
    var Ctor = Vue.extend(Main)
    new Ctor().$mount('#app')
