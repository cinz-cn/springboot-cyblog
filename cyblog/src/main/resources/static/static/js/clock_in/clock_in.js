var Main = {
    data() {
        return {
            items:{
                id: '',
                date: '',
                status: '',
            }
        };
    },
    created:function (){
        this.showDateWidget()
    },
    methods: {
        //日期选择控件
        showDateWidget:function (){
            let _this = this;
            layui.use('laydate', function(){
                var laydate = layui.laydate;
                var nowTime = new Date().valueOf();
                function clockInfo(obj){
                    // var i = ['2021-10-2','2021-10-3','2021-10-4'];
                    // var k = ['2021-10-5','2021-10-6','2021-10-7'];
                    // for (let j=0;j<=i.length;j++){
                    //     $('td[lay-ymd='+ i[j] +']').append("<div class=\"success\">已签</div>");
                    // }
                    // for (let c=0;c<=k.length;c++){
                    //     $('td[lay-ymd='+ k[c] +']').append("<div class=\"error\">未签</div>");
                    // }
                    $.ajax({
                        url: '/clock_in/showData',
                        data:{
                            "date":obj
                        },
                        method: '',
                        success:function (data){
                            if (data.success){
                                _this.items = data.data.items
                                for (let i=0;i<_this.items.length;i++){
                                    if (_this.items[i].status == 1){
                                        $('td[lay-ymd='+ _this.items[i].date +']').append("<div class=\"success\">已签</div>");
                                    }
                                    if(_this.items[i].status == 0){
                                        $('td[lay-ymd='+ _this.items[i].date +']').append("<div class=\"error\">未签</div>");
                                    }
                                }
                            }else{
                                alert(data.message)
                            }
                        }
                    })
                }
                //直接嵌套显示
                laydate.render({
                    elem: '#test-n1'
                    ,position: 'static'
                    ,theme: '#16a860' //自定义颜色主题
                    ,type: 'datetime'
                    ,format: 'yyyy-MM-dd'
                    ,showBottom: false
                    ,min: '2021-10-1'
                    ,max: nowTime //最大时间范围
                    ,change:function (value, date, endDate){//日期时间被切换后的回调
                        clockInfo()
                    }
                    ,mark: { //标注重要日子  已签  未签
                    }
                    ,done: function(value, date, endDate){
                    }
                    ,ready: function(date){//控件初始打开的回调
                        //choosedate.append("<div class=\"price\">1</div>");//在选中的td中加入显示价价格的div
                        // $("td[lay-ymd='2021-10-1']").append("<div class=\"price\">1</div>");
                        clockInfo()
                    }
                });
            });
        }
    }
}
var Ctor = Vue.extend(Main)
new Ctor().$mount('#clock_in')





